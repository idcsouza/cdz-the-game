<div class="box_top">IMPORTANTE</div>
<div class="box_middle">
<div class="aviso"><b>Nova Fun&ccedil;&atilde;o para Recupera&ccedil;&atilde;o de Senha</b><br />Utilize esta fun&ccedil;&atilde;o para recuperar sua senha sempre que quiser.<br />Ap&oacute;s definir a pergunta secreta e a resposta, as mesmas ficar&atilde;o gravadas e jamais ser&atilde;o alteradas.<br />Clique <a href="?p=config">aqui</a> para definir uma pergunta e resposta secreta.<br />Caso j&aacute; tenha feito, sugerimos que atualize a informa&ccedil;&atilde;o, e deixe sua conta mais segura!</div>
</div>
<div class="box_bottom"></div>