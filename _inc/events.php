<?php require_once('trava.php'); ?>
<script language = "JavaScript">
function abrir(){
	window.open("pixel.php",",","status=no,resizable=no,scrollbars=no,menubar=no,width=460,height=366,left=150,top=100");
}
</script>
<div class="box_top">Eventos</div>
<div class="box_middle">A cada dia da semana ocorrer&aacute; um evento diferente. Abaixo est&atilde;o todos os eventos programados pela CDZ - The Game, o dia da semana em que o evento acontecer&aacute;, e a descri&ccedil;&atilde;o de cada um.
  <div class="sep"></div>
	<div<?php if(date('N')==1) echo ' style="background:#323232;"'; ?>>
	<img src="_img/star.png" align="absmiddle" /><b>Segunda-Feira:</b> Pixel Premiado!
    <div class="sub2" style="margin-left:16px;text-align:left;">Neste evento, o usu&aacute;rio dever&aacute; encontrar o pixel que cont&eacute;m um pr&ecirc;mio, em uma imagem. Para quem ainda n&atilde;o sabe, pixel &eacute; o menor ponto que forma uma imagem digital, e o conjunto de pixels forma uma imagem. Uma imagem ser&aacute; exibida (de tamanho 400x225 pixels, resultando em 90.000 pixels), sendo que em um destes pixels estar&aacute; escondido o pr&ecirc;mio do evento (que poder&aacute; incluir uma VIP de 1 m&ecirc;s).<?php if(date('N')<>8){ ?><br /><a href="pixel.php" target="_blank">Clique aqui para acessar o evento.</a><?php } ?></div>
    </div>
    <div class="sep"></div>
    <div<?php if(date('N')==2) echo ' style="background:#323232;"'; ?>>
    <img src="_img/star.png" align="absmiddle" /><b>Ter&ccedil;a-Feira:</b> Reconhecendo o Apollo!
    <div class="sub2" style="margin-left:16px;text-align:left;">&Agrave;s ter&ccedil;as, teremos um evento simples, Apolo resolveu ser humilde uma vez na semana e estar&aacute; presenteando aquele que descobrir sua verdadeira imagem entre as 3 . O pr&ecirc;mio para o vencedor ser&aacute; o triplo do valor apostado.<?php if(date('N')==8) echo '<br /><a href="?p=bunshin" target="_blank">Clique aqui para acessar o evento.</a>'; ?></div>
</div>
    <div class="sep"></div>
    <div<?php if(date('N')==3) echo ' style="background:#323232;"'; ?>>
    <img src="_img/star.png" align="absmiddle" /><b>Quarta-Feira:</b> Ataque Dos Titans.
    <div class="sub2" style="margin-left:16px;text-align:left;">Toda quarta colocaremos alguns Tit&atilde;ns entre os players. Todos eles come&ccedil;aram o dia com uma alta quantia de yens. E voc&ecirc;s poderam ataca-los normalmente. Alguns ser&atilde;o mais forte que outros. E essa for&ccedil;a ir&aacute; influenciar na quantidade de yens que eles possuiram!</div>
    </div>
    <div class="sep"></div>
    <div<?php if(date('N')==4) echo ' style="background:#323232;"'; ?>>
    <img src="_img/star.png" align="absmiddle" /><b>Quinta-Feira:</b> Evento a decidir
    <div class="sub2" style="margin-left:16px;text-align:left;">-</div>
    </div>
    <div class="sep"></div>
    <div<?php if(date('N')==5) echo ' style="background:#323232;"'; ?>>
    <img src="_img/star.png" align="absmiddle" /><b>Sexta-Feira:</b> Evento a decidir
    <div class="sub2" style="margin-left:16px;text-align:left;">-</div>
    </div>
    <div class="sep"></div>
    <div<?php if(date('N')==6) echo ' style="background:#323232;"'; ?>>
    <img src="_img/star.png" align="absmiddle" /><b>S&aacute;bado:</b> Experi&ecirc;ncia em Dobro!
    <div class="sub2" style="margin-left:16px;text-align:left;">Talvez o dia da semana mais alvejado pelos cavaleiros. Aos s&aacute;bados, a experi&ecirc;ncia ganha em miss&otilde;es e treinos por tempo ser&aacute; dobrada! Sua evolu&ccedil;&atilde;o neste dia poder&aacute; superar qualquer expectativa!</div>
    </div>
    <div class="sep"></div>
    <div<?php if(date('N')==7) echo ' style="background:#323232;"'; ?>>
    <img src="_img/star.png" align="absmiddle" /><b>Domingo:</b> Yens ao Extremo!
    <div class="sub2" style="margin-left:16px;text-align:left;">Domingo &eacute; o dia de ganhar yens! Todas as miss&otilde;es e treinos por tempo lhe dar&atilde;o um adicional de 30% em yens (referente ao total de yens ganhos na miss&atilde;o/treino).</div>
    </div>
</div>
<div class="box_bottom"></div>