<div class="box_top">FAQ</div>
<div class="box_middle">Abaixo est&aacute; o FAQ da &quot;CDZ - The Game&quot;. As perguntas mais frequentes de nossos jogadores est&atilde;o respondidas abaixo, classificadas por categoria, para f&aacute;cil localiza&ccedil;&atilde;o. Caso sua d&uacute;vida n&atilde;o esteja entre estas, voc&ecirc; pode nos utilizar nosso <a href="../?=contact">formul&aacute;rio de contato</a>, ou ent&atilde;o utilizar a comunidade do orkut, onde v&aacute;rios jogadores poder&atilde;o respond&ecirc;-la rapidamente.
  <div class="sep"></div>
	<fieldset><legend>Minha Conta</legend>
    	<img src="_img/star.png" align="absmiddle" /><b>Quantas contas posso ter no CDZ - The Game?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">Cada jogador pode ter apenas 1 (uma) conta no jogo. Ao criar contas m&uacute;ltiplas, o jogador estar&aacute; desrespeitando a principal regra do jogo, e ser&aacute; punido assim que descoberto.</div>
        <div class="sep"></div>
        <img src="_img/star.png" align="absmiddle" /><b>Posso compartilhar minha conta com outra pessoa?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">Sim, por enquanto &eacute; permitido compartilhar sua conta com outros jogadores.</div>
        <div class="sep"></div>
        <img src="_img/star.png" align="absmiddle" /><b>Perdi minha senha, o que fa&ccedil;o agora?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">Utilize nosso formul&aacute;rio para cadastrar uma nova senha para sua conta, clicando <a href="?p=recover">aqui</a>. Ser&aacute; necess&aacute;rio informar o email utilizado no momento do cadastro, sendo que a nova senha ser&aacute; enviada para ele.</div>
        <div class="sep"></div>
        <img src="_img/star.png" align="absmiddle" /><b>Minha conta pode ser deletada por inatividade?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">Por enquanto n&atilde;o. Como o jogo e novo, n&atilde;o temos muitas contas em inatividade. Mas caso se torne um problema para os players poderemos sim aderir um certo tipo de limpeza.</div>
    </fieldset>
    <fieldset>
    	<legend>Organiza&ccedil;&otilde;es</legend>
    	<img src="_img/star.png" align="absmiddle" /><b>O que s&atilde;o as Organiza&ccedil;&otilde;es?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">As organiza&ccedil;&otilde;es s&atilde;o grupos de cavaleiros que se unem para realizarem miss&otilde;es, com a inten&ccedil;&atilde;o de aumentar a reputa&ccedil;&atilde;o de seus nomes no mundo.</div>
        <div class="sep"></div>
        <img src="_img/star.png" align="absmiddle" /><b>Em que as organiza&ccedil;&otilde;es s&atilde;o &uacute;teis?</b>
        <div class="sub2" style="margin-left:16px;text-align:left;">Elas aumentam os atributos de sua conta, dependendo do n&iacute;vel da organiza&ccedil;&atilde;o. Para cada n&iacute;vel, voc&ecirc; ganha 1 ponto de atributo em For&ccedil;a, Conhecimento e T&eacute;cnica..</div>
        <div class="sep"></div>
        <img src="_img/star.png" align="absmiddle" /><b>Para que servem as doa&ccedil;&otilde;es?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">As doa&ccedil;&otilde;es servem para ampliar as instala&ccedil;&otilde;es da sede de sua organiza&ccedil;&atilde;o, ajudando na evolu&ccedil;&atilde;o do mesmo, como ganho em reputa&ccedil;&atilde;o.</div>
        <div class="sep"></div>
    </fieldset>
	<fieldset><legend>Batalha</legend>
    	<img src="_img/star.png" align="absmiddle" /><b>Qual a equa&ccedil;&atilde;o utilizada para calcular os danos f&iacute;sicos?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">A equa&ccedil;&atilde;o que o sistema utiliza no momento &eacute; a seguinte:<br />
    	<b>(( For&ccedil;a * (( For&ccedil;a * 5) / T&eacute;cnica do Inimigo ) / 4 ) / 4 ) + B&ocirc;nus de 1 a 10</b><br />
    	Se a diferen&ccedil;a entre a sua For&ccedil;a e a T&eacute;cnica do inimigo &eacute; maior que 25, ent&atilde;o o dano causado sobre ele &eacute; nulo.<br />A equa&ccedil;&atilde;o acima pode mudar a qualquer momento.</div>
    </fieldset>
    <fieldset><legend>Geral</legend>
    	<img src="_img/star.png" align="absmiddle" /><b>Quais s&atilde;o os crit&eacute;rios de ordena&ccedil;&atilde;o do Ranking?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">Os crit&eacute;rios s&atilde;o, na ordem: N&iacute;vel, Vit&oacute;rias, Yens Faturados e Derrotas.</div>
        <div class="sep"></div>
        <img src="_img/star.png" align="absmiddle" /><b>Como libero mais personagens?</b>
    	<div class="sub2" style="margin-left:16px;text-align:left;">A id&eacute;ia inicial era liberar por miss&otilde;es, mas foi decidido que, a cada n&iacute;vel, novos personagens estar&atilde;o dispon&iacute;veis. Voc&ecirc; pode visitar o com&eacute;rcio para adquirir os novos personagens, sem pagar por nada.</div>
    </fieldset>
</div>
<div class="box_bottom"></div>