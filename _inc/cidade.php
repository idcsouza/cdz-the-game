<div id="city" style="display:none;">
<div class="box_top">Mapa da Cidade [<a href="#" onClick="document.getElementById('city').style.display='none'">Fechar</a>]</div>
<div class="box_middle">
	<script>
	function nome(nom){
		document.getElementById('divnome').innerHTML=nom;
	}
	function nomeout(){
		document.getElementById('divnome').innerHTML='<b>Passe o mouse sobre um ponto no mapa para visualizar sua descri&ccedil;&atilde;o.</b>';
	}
	</script>
	<div class="modalExemplo" style="background:#282828;padding-top:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px;">
		<div style="background:url(_img/city/city.jpg) no-repeat center;width:505px;height:315px;">
			<a href="?p=ramen"><img src="_img/city/point.png" border="0" style="position:relative; left:383px; top:243px;" onmouseover="nome('<b>Hospital</b>: Entre e cure todos os seus ferimentos')" onmouseout="nomeout()" /></a>
			<a href="?p=missions"><img src="_img/city/point.png" border="0" style="position:relative; left:171px; top:150px;" onmouseover="nome('<b>Sal&atilde;o dos Deuses</b>: Realize miss&otilde;es em nome de seu Deus, e ganhe recompensas!')" onmouseout="nomeout()" /></a>    
			<a href="?p=school"><img src="_img/city/point.png" border="0" style="position:relative; left:123px; top:173px;" onmouseover="nome('<b>Treinamento</b>: Aprenda e aperfei&ccedil;oe ataques com os melhores Mestres')" onmouseout="nomeout()" /></a>
			<a href="?p=hunt"><img src="_img/city/point.png" border="0" style="position:relative; left:287px; top:59px;" onmouseover="nome('<b>Batalhas</b>: Procure e enfrente cavaleiros de todo o mundo! Ou simplesmente treine um pouco.')" onmouseout="nomeout()" /></a>
			<a href="?p=<?php if($db['orgid']>0) echo 'my'; ?>org"><img src="_img/city/point.png" border="0" style="position:relative; left:287px; top:167px;" onmouseover="nome('<?php if($db['orgid']>0) echo '<b>Minha Organiza&ccedil;&atilde;o</b>: Visite sua organiza&ccedil;&atilde;o atual!'; else echo '<b>Organiza&ccedil;&atilde;o</b>: Visualize as organiza&ccedil;&otilde;es existentes!'; ?>')" onmouseout="nomeout()" /></a>
			<a href="?p=shop"><img src="_img/city/point.png" border="0" style="position:relative; left:87px; top:233px;" onmouseover="nome('<b>Com&eacute;rcio</b>: Temos uma grande variedade de equipamentos!')" onmouseout="nomeout()" /></a>
        </div>
      <div class="sep"></div>
	  <div id="divnome" class="aviso"><b>Passe o mouse sobre um ponto no mapa para visualizar sua descri&ccedil;&atilde;o.</b></div>
	</div>
</div>
<div class="box_bottom"></div>
</div>