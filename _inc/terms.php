<div class="box_top">Termos e Condi&ccedil;&otilde;es de Uso do CDZ - The Game</div>
<div class="box_middle">A utiliza&ccedil;&atilde;o deste site fica sujeita aos termos e condi&ccedil;&otilde;es descritos abaixo. Ao prosseguir com o cadastro, o usu&aacute;rio estar&aacute; aceitando os Termos e Condi&ccedil;&otilde;es estipulados.<div class="sep"></div>
<fieldset><legend>I. SISTEMA</legend>
Este sistema/jogo foi totalmente desenvolvido por brasileiros, com o objetivo de divulga&ccedil;&atilde;o do anime Cavaleiro Dos Zodiacos. Todos os direitos do anime est&atilde;o reservados ao criador   
Masami Kurumada , incluindo os nomes e imagens dos personagens, com exce&ccedil;&atilde;o do sistema.
</fieldset>
<fieldset><legend>II. USU&Aacute;RIO</legend>
O usu&aacute;rio do site tem direito a uma conta no jogo <b>CDZ - The Game</b>, de uso totalmente pessoal e particular. Cada usu&aacute;rio tem a responsabilidade de manter sua conta em seguran&ccedil;a, utilizando de todos os meios oferecidos pelo sistema/jogo. Nenhum membro da equipe de administra&ccedil;&atilde;o do jogo possui acesso &agrave;s senhas dos usu&aacute;rios.
</fieldset>
<fieldset><legend>III. REGRAS</legend>
1. Fica proibido qualquer tipo de propaganda de outro RPG ou anime atrav&eacute;s deste site;<br />
2. Fica proibido a venda ou transfer&ecirc;ncia de conta de um usu&aacute;rio para outro, tendo como puni&ccedil;&atilde;o o banimento da conta;<br />
3. Fica proibido qualquer jogador se passar por membro da administra&ccedil;&atilde;o do jogo (Resultando em banimento imediata da conta.);<br />
4. Fica proibido utilizar nomes de usu&aacute;rio ilegais, ofensivos ou que contenham frases de insulto ou racismo de qualquer esp&eacute;cie, tendo como puni&ccedil;&atilde;o o banimento da conta;<br />
5. Fica proibido o uso de contas m&uacute;ltiplas para o mesmo usu&aacute;;rio, com o risco de banimento de todas as contas.
</fieldset>
<div class="sep"></div>
<div class="aviso">Voc&ecirc; concorda com os termos descritos acima?</div>
<div class="sep"></div>
<div align="center"><input type="button" class="botao" value="Sim" onclick="location.href='?p=reg'" /> <input type="button" class="botao" value="N&atilde;o" onclick="location.href='?p=login'" /></div>
</div>
<div class="box_bottom"></div>