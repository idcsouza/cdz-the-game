<div class="box_top">Publicidade</div>
<div class="box_middle">
	Nosso site est&aacute; crescendo a cada dia, e com isso, o n&uacute;mero de usu&aacute;rios aumenta constantemente. Temos orgulho em saber que nossa dedica&ccedil;&atilde;o esteja obtendo retorno, e queremos compartilhar isso com voc&ecirc;, anunciante! A &quot;CDZ - The Game&quot; preparou algumas &aacute;reas para an&uacute;ncios diversos, onde voc&ecirc; poder&aacute; vender seu produto ou servi&ccedil;o, com o objetivo de melhorar sua receita mensal. Est&aacute; interessado em anunciar? Veja abaixo tudo que voc&ecirc; precisa saber para ser um anunciante!
	  <div class="sep"></div>
    - No momento, temos duas &aacute;reas espec&iacute;ficas para anunciar (topo do conte&uacute;do e fim do conte&uacute;do);<br />- Voc&ecirc; pode realizar quantos an&uacute;ncios desejar;<br />- Cada an&uacute;ncio ter&aacute; uma rota&ccedil;&atilde;o de 15 dias, ou seja, seu an&uacute;ncio terminar&aacute; ap&oacute;s ser exibido por 15 dias;<br />- Os an&uacute;ncios ser&atilde;o exibidos para todos os jogadores que n&atilde;o possuem conta VIP, e para os usu&aacute;rios que n&atilde;o estiverem logados no site (visitantes);<br />
    - O sistema de anuncio &eacute; unico. Somente o seu an&uacute;ncio ser&aacute; exibido durante todo o periodo contratado.
    <div class="sep"></div>
    <table cellspacing="1" cellpadding="0" width="100%">
    	<tr class="table_dados" style="background:#323232;" onmouseover="style.background='#2C2C2C'" onmouseout="style.background='#323232'">
        	<td>An&uacute;ncio no Topo do Conte&uacute;do</td>
            <td>R$ 15,00 / 15 dias</td>
        </tr>
        <tr class="table_dados" style="background:#323232;" onmouseover="style.background='#2C2C2C'" onmouseout="style.background='#323232'">
        	<td>An&uacute;ncio no Fim do Conte&uacute;do</td>
            <td>R$ 10,00 / 15 dias</td>
        </tr>
    </table>
    <div class="sep"></div>
    Caso haja interesse, favor envie um email para contato@cdzthegame.com, juntamente com o planejamento do an&uacute;ncio (incluindo a posi&ccedil;&atilde;o em que o mesmo ir&aacute; ser fixado no site, e o conte&uacute;do do an&uacute;ncio).
    <div class="sep"></div>
    <div class="aviso">An&uacute;ncios de parceiros do site ganham desconto de R$ 5,00.</div>
</div>
<div class="box_bottom"></div>