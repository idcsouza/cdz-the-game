<?php
switch($db['doujutsu']){
	case 1: $txtdoujutsu='Toma'; $habilidades='- B&ocirc;nus de '.($db['doujutsu_nivel']*2).'% em Tecnica;<br />- 3 Ataques.'; break;
	case 2: $txtdoujutsu='Odisseu'; $habilidades='- B&ocirc;nus de '.($db['doujutsu_nivel']*2).'% em For&ccedil;a;<br />- 3 Ataques.'; break;
	case 3: $txtdoujutsu='Teseu'; $habilidades='- B&ocirc;nus de '.($db['doujutsu_nivel']*2).'% em Conhecimento;<br />- 3 Ataques.'; break;
}
?>
<div class="box_top">Meu Protetor</div>
<div class="box_middle">Informa&ccedil;&otilde;es do seu cavaleiro protetor.
  <div class="sep"></div>
	<table width="100%" cellpadding="0" cellspacing="1">
    <tr class="table_dados" style="background:#323232;">
        <td width="220"><img src="_img/doujutsus/<?php echo strtolower($txtdoujutsu); ?>.jpg" onmouseover="Tip('<div class=tooltip><?php echo $txtdoujutsu; ?></div>')" onmouseout="UnTip()" />
        <?php
        if($db['doujutsu']==1){
			if($db['doujutsu_nivel']<=6) $txtdoujutsu.=' - N&iacute;vel 1';
			if(($db['doujutsu_nivel']>6)&&($db['doujutsu_nivel'])<=12) $txtdoujutsu.=' - N&iacute;vel 2';
			if(($db['doujutsu_nivel']>13)&&($db['doujutsu_nivel'])<=18) $txtdoujutsu.=' - N&iacute;vel 3';
			if(($db['doujutsu_nivel']>18)&&($db['doujutsu_nivel'])<=24) $txtdoujutsu.=' - N&iacute;vel 4';
			if($db['doujutsu_nivel']>24) $txtdoujutsu.=' - N&iacute;vel 5';
		}
		?></td>
        <td width="80"><b>N&iacute;vel <?php echo $db['doujutsu_nivel']; ?></b><?php if($db['doujutsu_nivel']<30){ ?><br /><span class="sub2">Experi&ecirc;ncia<br /><?php echo $db['doujutsu_exp'].' / '.$db['doujutsu_expmax']; ?></span><?php } else { ?><br /><span class="sub2">N&iacute;vel m&aacute;ximo alcan&ccedil;ado.</span><?php } ?></td>
        <td><span class="sub2"><?php echo $habilidades; ?><br />
        <?php
		if(date('Y-m-d H:i:s')<$db['vip']){
        	if($db['doujutsu']==1) echo '<a href="?p=changedoujutsu&new=2">- Trocar para Odisseu</a><br /><a href="?p=changedoujutsu&new=3">- Trocar para Teseu</a>';
			if($db['doujutsu']==2) echo '<a href="?p=changedoujutsu&new=3">- Trocar para Teseu</a><br /><a href="?p=changedoujutsu&new=1">- Trocar para Toma</a>';
			if($db['doujutsu']==3) echo '<a href="?p=changedoujutsu&new=2">- Trocar para Odisseu</a><br /><a href="?p=changedoujutsu&new=1">- Trocar para Toma</a>';
		}
		?></span></td>
    </tr>
    </table>
</div>
<div class="box_bottom"></div>
<script>
<?php if($db['doujutsu']==2){ ?>document.getElementById('atrtai').innerHTML=((document.getElementById('atrtai').innerHTML)*1)+<?php echo round($db['taijutsu']*($db['doujutsu_nivel']/50)); ?>;<?php } ?>
<?php if($db['doujutsu']==3){ ?>document.getElementById('atrnin').innerHTML=((document.getElementById('atrnin').innerHTML)*1)+<?php echo round($db['ninjutsu']*($db['doujutsu_nivel']/50)); ?>;<?php } ?>
<?php if($db['doujutsu']==1){ ?>document.getElementById('atrgen').innerHTML=((document.getElementById('atrgen').innerHTML)*1)+<?php echo round($db['genjutsu']*($db['doujutsu_nivel']/50)); ?>;<?php } ?>
</script>